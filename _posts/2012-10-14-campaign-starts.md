---
layout: post
cover: false
title: The Campaign Starts!
date: 2012-10-14 00:01:01
tags: news campaign
subclass: 'post tag-campaign'
categories: 'PhiRho'
navigation: True
logo: 'assets/images/fp.png'
---

The Freeze Phil Campaign formally starts. I am having discussions with close
and extended family, colleagues and professionall advisors about how to enlarge
the campaign and to gather support from a much wider group of people in
Australia (initially). I think the campaign should be targeting politicians,
legal people, ethicists and the wider general public. I will set up a private
mailing list for these discussions - more appropriate people can be added to
this list as required. I will probably set up a separate announcement list for
people who are interested in keeping up with the progress of the campaign but
who are not actively involved.


