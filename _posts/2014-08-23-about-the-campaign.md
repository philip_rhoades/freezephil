---
layout: post
cover: false
title: About the Campaign
date: 2014-08-23 00:01:01
tags: news campaign
subclass: 'post tag-campaign'
categories: 'PhiRho'
navigation: True
logo: 'assets/images/fp.png'
---


## Disclaimer

Firstly, this IS NOT a site promoting Voluntary Euthanasia (VE) - although I am
personally in favour of allowing intelligent, rational adults the option of VE for
their own circumstances. It may be, however, that people who have thought about
VE might find some different perspectives here.

## What is this site about then?

It is a long story and has taken many decades of life exerience, social
activism of various sorts and many frustrations with the state of Western
"Civilisation" to finally come to the conclusion that, in this case, I must DO
something rather than keeping my head below the parapet. I am extremely unhappy
about how current legislation in most Western countries (including Australia)
affects people when they want to consider other options to a "natural" death.
I believe, in a really civilised society, not just one that pretends to be (one
who's culture is too affected by superstition and not enough by reasoned and
rational discussion), that we would treat humans humanely and not enforce a
miserable and degrading existence in the last years and months of their lives.
I think this is particularly true when there is the possibility of a
(admittedly experimental) medical procedure that might allow a revival to good
health at some stage in the future - sort of like someone in an ambulance going
to hospital for treatment - but on a much longer time scale. I, as a rational,
still intelligent, aware adult, should be allowed to take a medical gamble on a
risky scientific experiment - if I so desire - and not have some Bronze Age
view of the world imposed upon me which severely limits my options.

## In the beginning . .

<b>Life is too short!</b> No doubt about it, there is just not enough time
(even if you are lucky enough to get your "three score and ten years" or more)
to experience everything that the universe has to offer.  My world view is
summarised on my Sydney Futurists page:

"Since I first started getting actively involved in Cryonics (Cryonics
Association Of Australasia) I have become convinced that it is not sufficient
to just want to have an extended, wonderful life for myself.  It was clear that
for my personal dreams to have any chance of success that there would have to
be some preconditions. If I could not extend a vigorous and worthwhile life in
the current instance, and I eventually required a Cryonic Suspension, then if
there was going to be any chance of a reanimation of my frozen self in the
future, then that was going to require, at least, an advanced technological
civilisation that was capable of the reanimation. That fundamental requirement
raises issues that go beyond issues of immediate self-interest.

I grew up in a scientific family in the fifties, sixties and seventies - during
a period of great optimism. Western economies were booming, we sent people to
the moon and returned them to Earth, scientific and medical advances (eg the
first heart transplant) were gathering pace. However, there were some dark
clouds. We were involved in stupid and illegal wars in South East Asia and
there were some serious and rapidly growing problems with damage to the natural
world - caused, basically, by the rapid expansion of human populations and
material consumption.

Humans are clearly going to have to make some existential choices in the near
future. The results of the those choices could be, in the extremes, that our
species goes on to explore the rest of the universe for millennia to come, or,
at the other extreme, complex life on Earth could vanish altogether. I think it
is clear that personal survival and growth fundamentally depends on the
sociological and biological health of our little planet. If we want to reach an
incredibly exciting and interesting future for ourselves we are going to need a
good dose of enlightened self-interest for ourselves, and more concern for
other people and for other species on this planet.

When wars were fought with clubs and spears etc it did not matter too much that
some numbers of people removed themselves from the human gene pool. This is not
true for the modern world. When bad governments, bad leaders or even just crazy
individuals committed to one fairy story or another are capable of returning
our species to the Stone Age - or worse, we need to be much more intelligent
about how we run the place. I guess this Group is an attempt to have
discussions with people who have desires about reaching or seeing a wonderful
future and are interested in having discussions/arguments about the best way to
get there!"

## The Big Problems preventing a significant increase in life-spans

Developing countries have their own significant problems to deal with. My view
is, in the developed West, that we have related but more specific problems. As
a biologist with an interest in Information Technology (IT) and a student of
politics, I think our problems resolve down to a few major issues where the
West has to start showing some leadership:

- Overpopulation
- Overconsumption in the West and increasingly in developing countries
- The power of superstitious elites
- The lack of influence of evidence-based decision making (a corollary of the previous point)
- Pitiful global conflict resolution mechanisms
- Legislative change cannot keep up with scientific and technological change

## Summary

<b>
We need legislative change to allow other end of (current) life options ie
specifically, "Pre-Mortem Cryonic Suspensions"

</b>




