---
layout: post
cover: false
title: The Logic for Pre-Mortem Cryonic Suspensions
date: 2014-08-24 00:01:01
tags: info
subclass: 'post tag-info'
categories: 'PhiRho'
navigation: True
logo: 'assets/images/fp.png'
---

## This may resemble a "Thought Experiment" but here it is:

If you subscribe to the idea that a person is mostly what is in their neural
tissue and structure (I do) - compared to what is in the rest of their anatomy
and physiology, then if you are interested in preserving the person in your
current life, you will look after your health generally and your brain in
particular.  However, if you are not young anymore, and not likely to be part
of a generation that has access to extended, vigourous, healthy, good life spans
through new technologies - then you are likely to need one of the other (last
resort) options for another chance at an extended life. You will be restricted
to some sort of preservation of the person that is you via a Cryonics
Suspension, Neural Archive or possibly some other technologies that will allow
copying of neural architecture (hopefully in the not too distant future).

Ideally, it would be good to be born, grow and learn rapidly until maturity but
then to continue to grow and learn and remember the things that you want to
remember - FOREVER - or for at least as long as you think life is still worth
living. Below is a graph that expresses this idea.

![alt text](/assets/images/Age-Cognition_Ideal.png "Age Cognition Ideal")

Graph 1. An "Ideal" Age vs Cognitive Power + Total Storage Scenario

- Consider the slope of the graph as the "Cognitive Power" of a human brain. A
baby is like a sponge for information - it is born with many more neural
connections than it needs and while it is growing and beginning to understand
the world, it's neural architecture is also furiously growing, making new
connections and seriously pruning unneeded neurones and connections. So the
baby's CP is very high and decreases as one reaches maturity - ideally it would
be good, if this CP never went into negative territory.
- Consider the height of the graph at any point as the "Total Storage" of a human
brain ie all accumulated memories, experiences and expertise etc that has not
been irretrievably lost for some reason (eg irrelevance - your grocery shopping
list from five months ago). So TS at any point in time will be the TS from the
previous point, less information that has been lost but including newly stored
information. As long as TS keeps increasing, you are in good shape for
remembering most of the important stuff in your life and although there is
change, there is also continuity - so you "feel" like the "same" person.
- Consider the far right of the graph, when the graph drops to zero, as the point
of death. If one has to die, it would be good if it was after a long,
productive, interesting and exciting life - and it would be good if death
happened quickly with no terrible, extended period of increasing physical
decrepitude and dramatic loss of CP and TS.
- If your body / neural tissue can be preserved at low temperatures very quickly
after death, then much of this CP and TS is recoverable (arguably) with future
technology.

However, this "Ideal" situation currently does not happen in the real world and
there are a number of problems with achieving this scenario.

Here is a more realistic "Best Case" view of things:

![alt text](/assets/images/Age-Cognition_Actual_BestCase.png "Age Cognition Actual Best Case")

Graph 2. A Realistic "Best Case" Age vs Cognitive Power + Total Storage Scenario

In this situation, CP and TS DO actually start decreasing after about age 25.
Generally, some of the lost CP and TS is made up by "Experience" (the red area)
that one gets from surviving in the real world. For example, in a conversation
you might not be able to immediately recall the word "indifferent" - as in "The
Universe is neither conducive nor hostile to life - it is indifferent to life."
If you can't remember the word "indifferent" you will probably be able to think
of another word that expresses the same sentiment and the person you are
talking to will be able to understand exactly what you mean.

So in this situation, you remain in pretty good shape until the day you die,
quickly, at an advanced age.

However, if you are very unlucky this is how the graph might look for you:

![alt text](/assets/images/Age-Cognition_Actual_WorstCase.png "Age Cognition Actual Worst Case")

Graph 3. A Realistic "Worst Case" Age vs Cognitive Power + Total Storage Scenario

In this situation you might be afflicted with an early-onset dementia of one
sort or another or even some other terrible brain destroying disease like
cancer, so, by the time you die, there might be very little information worth
preserving in your neurology. This is a terrible situation and one that the
world should be putting more research and development into resolving.

The following graph probably describes the situation for most people and allows
me to actually get to the analysis of the problem that I want to consider:

![alt text](/assets/images/Age-Cognition_Actual_Typical.png "Age Cognition Actual Typical")

Graph 4.  A Realistic "Typical Case" Age vs Cognitive Power + Total Storage Scenario

In the "Typical Case", the situation is somewhere between the "Best" and
"Worst" Cases and will vary from individual to individual. So, this is the
crunch, if you want your neural architecture preserved, it is likely that the
best way to preserve as much of the meaningful information in your brain as
possible will, by necessity, force you to think about doing that preservation
IN ADVANCE of a "natural" death.  That is, at a time that you judge is a good
compromise between maximising the length of your current life / incorporation
with the need to maximise the amount of useful information in your brain that
can be preserved for future revival / re-incorporation / posterity.

The problem is, YOU ARE NOT ALLOWED TO DO THIS! In most Western jurisdictions,
if you try a "Pre-Mortem Cryonic Suspension" you would be attempting suicide
or, if some family / friends / colleagues / professional people help you, they
will be committing one or more crimes with potentially severe penalties. This
is an utterly stupid and ridiculous situation and desperately needs changing.
Rational adults, who have, for one reason or another, decided that life is no
longer worth living, should not be prevented from terminating their current
existence either permanently or with a view to an improved existence at some
time in the future. On Graph 4, the position of PMCS should be up to the sole
discretion of the person themselves.


