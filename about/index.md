---
layout: page
title: About
class: 'post'
navigation: True
logo: 'assets/images/fp.png'
current: about
---

The Freeze Phil Campaign has been incubating for some time but was formally
started, finally, in October 2012.

The goal of this project is a legal, "Pre-Mortem Cryonic Suspension" for Phil
at an appropriate time and place for him and of his own choosing.  A brief bio
of Phil:

Philip Rhoades, BSc: Philip has a background in biomedical research and IT.  He
is completing a mature-age PhD in Population Genetics involved with computer
modelling of threatened populations. In general, Phil has had a good life for a
person living in a great time and place in history. He has his complaints about
some difficult life experiences - even tragedies - but has generally been a lot
better off than most other people in the history of our species and in other
parts of the world. But still, he thinks life is far too short.

Phil has a group of personal and professional advisors from whom he takes some
notice but all material on this web site is Phil's responsibility. The plan is
to build a substantial campaign to force legislative change that will allow
people more flexibility about their end of (current) life decisions ie in
short, to allow people a legal "Pre-Mortem Cryonic Suspension".

