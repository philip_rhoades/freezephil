---
layout: page
title: Frequently Asked Questions
class: 'post'
navigation: True
logo: 'assets/images/fp.png'
current: faq
---

- Why can't you just have a Cryonic Suspension after a "natural" death in the usual way?:

There are significant problems with that scenario and in any case, I believe it
is a human right to have control over my own life and the timing of my death. More later . .

- Since a physical revival from a Cryonic Suspension is unlikely, aren't you just
asking for Voluntary Euthanasia?:

No. More later . .

- Isn't it "un-natural" to want to live longer than "normal"?:

No. More later . .

- Isn't there a contradiction about complaining about overpopulation and wanting
longer life spans?:

No. More later . .

- Won't technology solve all the worlds problems?:<br>

No. More later . .

- You will need to change the legal definition of death - isn't doing that like
opening a Pandora's Box?:

Possibly but maybe not necessarily. More later . .

- Isn't decoding the information in the brain pie-in-the-sky?:

No but it will require major advances in technology - on the same scale as the
advances in DNA sequencing in the last couple of decades. More later . .



