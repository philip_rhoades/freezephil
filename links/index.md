---
layout: page
title: Some Interesting Links
class: 'post'
navigation: True
logo: 'assets/images/fp.png'
current: links
---

[Alcor](http://www.alcor.org/)

[Brain & Mind Research Institute](http://www.bmri.org.au)

[Cosmentis](http://cosmentis.org/)

[Cryonics Association of Australasia](http://cryonics.org.au)

[Novamente](http://www.novamente.net)

[Open Cognition Framework](http://opencog.org)

[Stasis Systems Australia](http://http://stasissystemsaustralia.com/)

[Sydney Futurists](http://www.meetup.com/SydneyFuturists/)

[The Australian Brain Bank](http://www.austbrainbank.org.au)

[The Blue Brain Project](http://bluebrain.epfl.ch/Jahia/site/bluebrain/op/edit/pid/18699)

[The Brain Dynamics Centre](http://www.brain-dynamics.net)

[The Brain Preservation Foundation](http://brainpreservation.org/)

[The Cryonics Institute](http://www.cryonics.org/)

[The Human Connectome Project](http://www.humanconnectomeproject.org)

[The Life Extention Foundation](http://www.lef.org/)

[The Neural Archives Foundation](http://neuralarchivesfoundation.org/)



